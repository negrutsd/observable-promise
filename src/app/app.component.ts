import { User } from './user.model';
import { ServiceService } from './service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  users: User[];

  constructor(private service: ServiceService) {}

  ngOnInit() {
    return this.service.getUser().subscribe(data => this.users = data);
  }

}
